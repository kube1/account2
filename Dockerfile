FROM node:10-alpine
EXPOSE 5000
WORKDIR /app
COPY package.json ./
RUN NODE_ENV=production npm install && npm cache clean --force
COPY . .
CMD ["node", "index.js"]
