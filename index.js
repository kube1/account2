const Hapi = require('@hapi/hapi')

const init = async () => {
  const server = Hapi.server({
    port: 5000
  });

  server.route({
    method: 'GET',
    path: '/',
    handler: (request, h) => {
      return `
      <h1>${server.info.uri}</h1>
      <h2>Account 2</h2>
      <h3>${process.env.NODE_ENV}</h3>
      <h4>${process.env.APP || 'undefined'}</h4>

      `;
    }
  });
  await server.start();
  console.log('Server running on %s', server.info.uri);

  return server;
};

process.on('unhandledRejection', (err) => {
  console.log(err);
  process.exit(1);
});

init();

